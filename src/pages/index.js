import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <h1>Thanks for stopping by.</h1>
    <p>I'm a community manager and software developer in Nashville.</p>
    <p>I'm also a mom and wife and cat mom and seamstress and I love Vermont.</p>
    <Link to="/page-2/">Contact Info</Link>
  </div>
)

export default IndexPage
