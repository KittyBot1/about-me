import React from 'react'
import Link from 'gatsby-link'

const SecondPage = () => (
  <div>
    <p>Twitter</p>
    <p>GitLab</p>
    <p>GitHub</p>

    <Link to="/">Return to homepage</Link>
  </div>
)

export default SecondPage
